package mars2052.logic.domain;

public enum TraitType {

    POSITIVE,
    NEUTRAL,
    NEGATIVE;

    TraitType() {
    }
}
